#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <Wire.h>
#include <Arduino.h>
#include <PubSubClient.h>
#include <Adafruit_MLX90614.h>
#include <SPI.h>
#include "SX1272.h"
//////////////////////////////////////// Change these values for each tag ////////////////////////////////////////

const char* uuid = "TAG_012345";    // Change UUID value to TAG_XXXXXX !
#define node_addr 10				        // LoRa Address of this tag

#define PREPARE_SEC 5	          	  // Preparing seconds before reading all values
#define sleepTimeS 10               // Sleep time in sec

//////////////////////////////////////// ADXL345 ////////////////////////////////////////
#define DEVICE (0x53) // Device address as specified in data sheet
#define ADXL345_MG2G_MULTIPLIER (0.004)
#define SENSORS_GRAVITY_STANDARD          (SENSORS_GRAVITY_EARTH)
#define SENSORS_GRAVITY_EARTH             (9.80665F)              /**< Earth's gravity in m/s^2 */

byte adxl_buff[6];
float adxl_value[3];
float adxl_cal[3] = {0, 0, 0};

char POWER_CTL = 0x2D;    //Power Control Register
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32;    //X-Axis Data 0

//////////////////////////////////////// MLX96014 ////////////////////////////////////////
Adafruit_MLX90614 mlx = Adafruit_MLX90614();

float temp_ambient = 0.0;
float temp_object = 0.0;

//////////////////////////////////////// GPS Module ////////////////////////////////////////

TinyGPS gps;
SoftwareSerial ss(2, 10);    // RX, TX
static void smartdelay(unsigned long ms);
float flat, flon;
unsigned long age;
  
char buf[100];
int count = 0;  


/////////////////////////////////////// InAir9 LoRa ///////////////////////////
#define FCC_US_REGULATION
#define MAX_DBM 14
#define BAND868
const uint32_t DEFAULT_CHANNEL = CH_10_868;

// #define WITH_APPKEY
// #define WITH_ACK

// CHANGE HERE THE LORA MODE
#define LORAMODE  1
#define DEFAULT_DEST_ADDR 1

#ifdef WITH_APPKEY
uint8_t my_appKey[4]={5, 6, 7, 8};
#endif

#define PRINTLN                   Serial.println("")
#define PRINT_CSTSTR(fmt,param)   Serial.print(F(param))
#define PRINT_STR(fmt,param)      Serial.print(param)
#define PRINT_VALUE(fmt,param)    Serial.print(param)
#define FLUSHOUTPUT               Serial.flush();

#ifdef WITH_ACK
#define NB_RETRIES 2
#endif

double temp;
char float_str[20];
uint8_t message[100];
int loraMode = LORAMODE;

/////////////////////////////////// Pulse Sensor ////////////////////////////////////
//  VARIABLES
int pulsePin = 0;                 // Pulse Sensor purple wire connected to analog pin 0
int blinkPin = 13;                // pin to blink led at each beat
int fadePin = 5;                  // pin to do fancy classy fading blink at each beat
int fadeRate = 0;                 // used to fade LED on with PWM on fadePin


// these variables are volatile because they are used during the interrupt service routine!
volatile int BPM;                   // used to hold the pulse rate
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // holds the time between beats, the Inter-Beat Interval
volatile boolean Pulse = false;     // true when pulse wave is high, false when it's low
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.


void setup() {
  delay(1000);
  Serial.begin(9600);
  Serial.print("\n\nUUID: ");  Serial.println(uuid);

  pinMode(blinkPin,OUTPUT);         // pin that will blink to your heartbeat!
  pinMode(fadePin,OUTPUT);          // pin that will fade to your heartbeat!
  interruptSetup();
  
  // Initialize LoRa Module
  // Power ON the module
  sx1272.ON();
  int e;
  // Set transmission mode and print the result
  e = sx1272.setMode(loraMode);
  Serial.print("Setting Mode: state "); Serial.println(e);
  // enable carrier sense
  sx1272._enableCarrierSense=true;
  // Select frequency channel
  e = sx1272.setChannel(DEFAULT_CHANNEL);
  Serial.print("Setting Channel: state "); Serial.println(e);
  // Set Power DBM
  e = sx1272.setPowerDBM((uint8_t)MAX_DBM);
  Serial.print("Setting Power: state "); Serial.println(e);
  // Set the node address and print the result
  e = sx1272.setNodeAddress(node_addr);
  Serial.print("Setting node addr: state "); Serial.println(e);
  Serial.println("SX1272 successfully configured.");

  // Initialize MLX temperature sensor...
  Serial.println("Beginning MXL96014...");
  mlx.begin(); 
  
  // Initialize Accelormeter...
  Wire.begin();
  initialize_accel();
  delay(1000);

}


void loop() {
  long startSend;
  long endSend;
  uint8_t app_key_offset=0;
  int e;
  
  if (count < PREPARE_SEC){
    Serial.println(PREPARE_SEC -count);
    count ++;
    smartdelay(1000);
  }
  else{
    // Read Accelerometer's values and display them.
    readAccel(false);
    Serial.print("-- Accelerometer's values ->  X: ");Serial.print(adxl_value[0]);
    Serial.print("  Y: ");Serial.print(adxl_value[1]);
    Serial.print("  Z: ");Serial.println(adxl_value[2]);
    
    delay(100);   // It is recommended to add delay between different I2C reads
    
    read_mlx96014();
	  Serial.print("\tAmbient = "); Serial.print(temp_ambient); Serial.println(" *F");
    Serial.print("\tObject = "); Serial.print(temp_object); Serial.println(" *F");
    delay(100);

    print_gps_data();
  
    // Publish sensor data
    sprintf(buf, "ID:%s,X:%d.%02d,Y:%d.%02d,Z:%d.%02d,TO:%d.%02d,TA:%d.%02d,H:%d,LA:%d.%04d,LO:%d.%04d", 
        uuid, 
        (int)adxl_value[0], abs((int)(adxl_value[0]*100)%100),
        (int)adxl_value[1], abs((int)(adxl_value[1]*100)%100),
        (int)adxl_value[2], abs((int)(adxl_value[2]*100)%100),
        (int)temp_object, abs((int)(temp_object*100)%100),
		    (int)temp_ambient, abs((int)(temp_ambient*100)%100),
        BPM,
        (int)flat, abs((int)(flat*1000000)%1000000),
        (int)flon, abs((int)(flon*1000000)%1000000));
        
//    Serial.println(buf);
    
	// Send message through LoRa
#ifdef WITH_APPKEY
      app_key_offset = sizeof(my_appKey);
      // set the app key in the payload
      memcpy(message,my_appKey,app_key_offset);
#endif
      uint8_t r_size;
      // then use app_key_offset to skip the app key
      r_size=sprintf((char*)message+app_key_offset, "\\!#%s", buf);

      Serial.print("Sending "); Serial.println((char*)(message + app_key_offset));
      Serial.print("Real payload size is "); Serial.println(r_size);
      int pl = r_size + app_key_offset;
      sx1272.CarrierSense();
      startSend = millis();

#ifdef WITH_APPKEY
      // indicate that we have an appkey
      sx1272.setPacketType(PKT_TYPE_DATA | PKT_FLAG_DATA_WAPPKEY);
#else
      // just a simple data packet
      sx1272.setPacketType(PKT_TYPE_DATA);
#endif

      // Send message to the gateway and print the result
      // with the app key if this feature is enabled
#ifdef WITH_ACK
      int n_retry=NB_RETRIES;
      do {
        e = sx1272.sendPacketTimeoutACK(DEFAULT_DEST_ADDR, message, pl);

        if (e==3)
          PRINT_CSTSTR("%s","No ACK");
        n_retry--;
        if (n_retry)
          PRINT_CSTSTR("%s","Retry");
        else
          PRINT_CSTSTR("%s","Abort");

      } while (e && n_retry);
#else
      e = sx1272.sendPacketTimeout(DEFAULT_DEST_ADDR, message, pl);
#endif
      endSend = millis();

      PRINT_CSTSTR("%s","LoRa pkt size ");
      PRINT_VALUE("%d", pl);
      PRINTLN;

      PRINT_CSTSTR("%s","LoRa pkt seq ");
      PRINT_VALUE("%d", sx1272.packet_sent.packnum);
      PRINTLN;

      PRINT_CSTSTR("%s","LoRa Sent in ");
      PRINT_VALUE("%ld", endSend-startSend);
      PRINTLN;

      PRINT_CSTSTR("%s","LoRa Sent w/CAD in ");
      PRINT_VALUE("%ld", endSend-sx1272._startDoCad);
      PRINTLN;

      PRINT_CSTSTR("%s","Packet sent, state ");
      PRINT_VALUE("%d", e);
      PRINTLN;
    count = 0;
  smartdelay(1000);

//  delay(1000); // a short delay to keep in loop while Deep Sleep is being implemented. - See more at: http://www.esp8266.com/viewtopic.php?f=29&t=2472#sthash.tubcdqXf.dpuf
  }
}

/*  -------------------------------- ADXL345 --------------------------------------------------- */
void initialize_accel(){
  Serial.println("-- Initializing ADXL345 module...");

  //ADXL345
  // i2c bus SDA = GPIO0; SCL = GPIO2
//  Wire.begin();

  // Put the ADXL345 into +/- 2G range by writing the value 0x01 to the DATA_FORMAT register.
  // FYI: 0x00 = 2G, 0x01 = 4G, 0x02 = 8G, 0x03 = 16G
  accel_write(DATA_FORMAT, 0x00);

  // Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
  accel_write(POWER_CTL, 0x08);

  // Get 10 values and calculate calibration values.
  uint8_t i = 0;
  uint8_t j = 0;
  for(i = 0; i < 11; i++)
  {
    readAccel(false);
    for (j = 0; j < 3; j++){
    adxl_cal[j] += adxl_value[j];
  }
    delay(100);
  }

  for (j = 0; j < 3; j++){
    adxl_cal[j] /= 10;
  }

  Serial.print("-- Calibration values of ADXL345 ->  X: ");Serial.print(adxl_cal[0]);
  Serial.print("  Y: ");Serial.print(adxl_cal[1]);
  Serial.print("  Z: ");Serial.println(adxl_cal[2]);

}

// Read X, Y, Z values and store to "adxl_value" global variable...
void readAccel(bool cal)
{
//  Serial.print("readAccel");
  uint8_t howManyBytesToRead = 6; //6 for all axes
  accel_read(DATAX0, howManyBytesToRead, adxl_buff); //read the acceleration data from the ADXL345
  short x = 0;
  x = (((short)adxl_buff[1]) << 8) | adxl_buff[0];
  adxl_value[0] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[3]) << 8) | adxl_buff[2];
  adxl_value[1] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[5]) << 8) | adxl_buff[4];
  adxl_value[2] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;

  if (cal == true)
    for (int j = 0; j < 3; j++){
      adxl_value[j] -= adxl_cal[j];
    }
}

void accel_write(byte address, byte val)
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // send register address
  Wire.write(val); // send value to write
  Wire.endTransmission(); // end transmission
}

// Reads num bytes starting from address register on device in to _buff array
void accel_read(byte address, int num, byte _buff[])
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // sends address to read from
  Wire.endTransmission(); // end transmission
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.requestFrom(DEVICE, num); // request 6 bytes from device

  int i = 0;
  while(Wire.available()) // device may send less than requested (abnormal)
  {
    _buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); // end transmission
}

/* ---------------------------------------- GPS -------------------------------- */
void print_gps_data(){
  gps.f_get_position(&flat, &flon, &age);  
  Serial.print("LAT: "); Serial.println(flat, 6);
  Serial.print("LON: "); Serial.println(flon, 6);
}

/* ---------------------------------------- MLX96014 -------------------------------- */
void read_mlx96014(){
  // Read and print out the temperature
//  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC()); 
//  Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
  temp_ambient = mlx.readAmbientTempF();
  temp_object = mlx.readObjectTempF();
  uint8_t count = 0;
  while (temp_ambient > 500){
    temp_ambient = mlx.readAmbientTempF();
    temp_object = mlx.readObjectTempF();
    if (count > 5)
      break;
  }
}

static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}
