#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void HRMonFunc(unsigned int din,long int ns,int *ir_ac_max,int *ir_ac_min,int *ir_2x_hbp, int *HRDRdy);
int avg_dc_est(long *p, unsigned int x);
int lp_fir_flt(int din);
int long mul16(int x, int y);

static const unsigned int FIRCoeffs[12] = {172,321,579,927,1360,1858,
									2390,2916,3391,3768,4012,4096};

void main()

{

unsigned int x,din;
int k;
int ir_2x_hbp;
int ir_ac_max=20;
int ir_ac_min=-20;
int HRDRdy=0;


unsigned int ir_dat[3000];		// IR Raw Data

	FILE *infile, *outfile;
	outfile = fopen("dout.txt","w");
	infile  = fopen("hrsamp.txt","r");

	for (k=0;k<3000;k++)
		{
			fscanf (infile, "%i",&x);
			ir_dat[k] = x;
		}

	fclose(infile);


	for (k=0; k<3000; k++)
	{
		din = ir_dat[k];

		HRMonFunc(din,(long)k,&ir_ac_max,&ir_ac_min,&ir_2x_hbp,&HRDRdy);

		fprintf(outfile,"%d,  %d,  %d,  %d\n",
					din,ir_ac_max,ir_ac_min,ir_2x_hbp);

	}

	fclose(outfile);
}
//
//	Heart Rate Monitor functions takes sample input 'din' and computes heart rate.
//	A new computed heart rate value is available when HRDRdy flag is set.
//	HRDRdy flag should be reset by the calling routine. Flag should be checked ~5 times a second.
//
//  ir_2x_hbp is proportional to 2x the heart beat period.
//	Heart rate is computed as 12000/ir_2x_hbp.
//	A running average of four samples is recommended for display on the screen.
//
void HRMonFunc(unsigned int din,long int ns,int *ir_ac_max,int *ir_ac_min,int *ir_2x_hbp,int *HRDRdy)
{
	static int ir_ac_sig_cur=0;
	static int ir_ac_sig_pre;
	static int ir_ac_sig_min=0;
	static int ir_ac_sig_max=0;
	static int ir_avg_est;

	static int pedge=0, nedge=0;
	static int pzxic=0, pzxip;
	static int nzxic=0, nzxip;

	static long ir_avg_reg=0;

//	Save current state
	ir_ac_sig_pre = ir_ac_sig_cur;

//	Process next data sample

	ir_avg_est = avg_dc_est(&ir_avg_reg, din);
	ir_ac_sig_cur = lp_fir_flt(din-ir_avg_est);


//	Detect positive zero crossing (rising edge)
	if ((ir_ac_sig_pre < 0) & (ir_ac_sig_cur >= 0))
		{

			*ir_ac_max = ir_ac_sig_max;
			*ir_ac_min = ir_ac_sig_min;

			pedge = 1; nedge = 0;
			ir_ac_sig_max = 0;
			pzxip = pzxic;
			pzxic = ns;

			if ((*ir_ac_max - *ir_ac_min)> 100 & (*ir_ac_max - *ir_ac_min)< 1000)
				{
					*ir_2x_hbp = (int)((pzxic - pzxip) + (nzxic - nzxip));
					*HRDRdy = 1;
				}
		}
//	Detect negative zero crossing (falling edge)
	if ((ir_ac_sig_pre > 0) & (ir_ac_sig_cur <= 0))
		{
			pedge = 0; nedge = 1;
			ir_ac_sig_min = 0;
			nzxip = nzxic;
			nzxic = ns;
		}
//	Find Maximum value in positive cycle
	if (pedge & (ir_ac_sig_cur > ir_ac_sig_pre))
		{	ir_ac_sig_max = ir_ac_sig_cur;	}

//	Find Minimum value in negative cycle
	if (nedge & (ir_ac_sig_cur < ir_ac_sig_pre))
		{	ir_ac_sig_min = ir_ac_sig_cur;	}
}

//	Average DC Estimator
int avg_dc_est(long *p, unsigned int x)
{
	*p += ((((long) x << 15) - *p)>>4);
    return (*p >> 15);
}

//	Low Pass FIR Filter
int lp_fir_flt(int din)
{
    static int cbuf[32];
    static int offset = 0;
    int long z;
    int i;

    cbuf[offset] = din;
    z = mul16(FIRCoeffs[11], cbuf[(offset - 11) & 0x1F]);
    for (i=0;i<11;i++)
	{
		z += mul16(FIRCoeffs[i], cbuf[(offset-i) & 0x1F] + cbuf[(offset-22+i) & 0x1F]);
	}
    offset = (offset + 1) & 0x1F;

    return  z >> 15;
}

//	Integer multiplier
int long mul16(int x, int y)
{
    return (long)x*(long)y;
}

