#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <Wire.h>
//#include <Adafruit_MLX90614.h>
#include <Arduino.h>
#include "algorithm.h"
#include "max30102.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* uuid = "TAG_012345";    // Change UUID value to TAG_XXXXXX !
#define UPLOAD_INTERVAL 30          // Uploading interval in seconds

//////////////////////////////////////// WiFi & MQTT ////////////////////////////////////////
const char* ssid = "TP-LINK_REAL3D";              // RPi3's AP name
const char* password = "dkanehahffk";       // RPi3's AP password
const char* mqtt_server = "192.168.1.110";    // Server has been built on the router(RPi 3) itself
const char* mqtt_topic = "ear_tag";

WiFiClient espClient;
PubSubClient client(espClient);

//////////////////////////////////////// MAXREFDES117# ////////////////////////////////////////
#define MAX_BRIGHTNESS 255

uint32_t aun_ir_buffer[100];       //infrared LED sensor data
uint32_t aun_red_buffer[100];     //red LED sensor data

int32_t n_ir_buffer_length;       //data length
int32_t n_spo2;                   //SPO2 value
int8_t  ch_spo2_valid;            //indicator to show if the SPO2 calculation is valid
int32_t n_heart_rate;             //heart rate value
int8_t  ch_hr_valid;              //indicator to show if the heart rate calculation is valid
uint8_t uch_dummy;
uint8_t tmp;
uint32_t un_min, un_max, un_prev_data, un_brightness;  //variables to calculate the on-board LED brightness that reflects the heartbeats
int i;
float f_temp;
bool b_first = true;
//int MAX_INT_PIN = 9;            //pin GPIO9 connects to the interrupt output pin of the MAX30102
int LED_PIN = 10;

//////////////////////////////////////// ADXL345 ////////////////////////////////////////
#define DEVICE (0x53) // Device address as specified in data sheet
#define ADXL345_MG2G_MULTIPLIER (0.004)
#define SENSORS_GRAVITY_STANDARD          (SENSORS_GRAVITY_EARTH)
#define SENSORS_GRAVITY_EARTH             (9.80665F)              /**< Earth's gravity in m/s^2 */

byte adxl_buff[6];
float adxl_value[3];
float adxl_cal[3] = {0, 0, 0};

char POWER_CTL = 0x2D;    //Power Control Register
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32;    //X-Axis Data 0

//////////////////////////////////////// MLX96014 ////////////////////////////////////////

//Adafruit_MLX90614 mlx = Adafruit_MLX90614();

float temp_ambient = 0.0;
float temp_object = 0.0;

//////////////////////////////////////// GPS Module ////////////////////////////////////////

TinyGPS gps;
SoftwareSerial ss(2, 0);    // RX, TX
static void smartdelay(unsigned long ms);
float flat, flon;
unsigned long age;
  
char buf[100];
int count = 0;  

void setup() {
  ESP.wdtDisable();
//  ESP.wdtEnable(WDTO_8S);
  delay(1000);
//  sprintf(uuid, "TAG_%08X", ESP.getChipId());
    
  Serial.begin(9600);
  Serial.print("UUID: ");  Serial.println(uuid);
  // Initialize WiFi and mqtt
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(mqtt_callback);
  
  // Initialize Accelormeter...
  Wire.begin();
  Wire.setClock(I2C_BUS_SPEED);
  initialize_accel();
  delay(1000);
  
  // Initialize MLX temperature sensor...
//  Serial.println("Beginning MXL96014...");
//  mlx.begin(); 
  pinMode(LED_PIN, OUTPUT);  
  // Initialize MAX30102
  Serial.println("Beginning MAX30102...");
  maxim_max30102_reset(); //resets the MAX30102
//  pinMode(MAX_INT_PIN, INPUT);  
  maxim_max30102_read_reg(REG_INTR_STATUS_1, &uch_dummy);     //Reads/clears the interrupt status register
  maxim_max30102_init();  
  b_first = true;
}

void setup_wifi() {
  
  int attempt = 0;
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
//    delay(500);
    
    digitalWrite(LED_PIN, LOW);
    delay(250);
    digitalWrite(LED_PIN, HIGH);
    delay(250);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
}

void reconnect() {
  // Loop until we're reconnected
  int attempt = 0;
  while (!client.connected()) {
    if (attempt < 3)
      attempt ++;
    else{
      Serial.println("Failed to connect to MQTT server, restarting...");
      ESP.restart();
    }
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(uuid)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}


void loop() {
  ESP.wdtFeed();
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (count < UPLOAD_INTERVAL){
    count ++;
    if (count %2 == 0)
      digitalWrite(LED_PIN, HIGH);
    else
      digitalWrite(LED_PIN, LOW);
  }
  else{
    // Read Accelerometer's values and display them.
    readAccel(false);
    Serial.print("-- Accelerometer's values ->  X: ");Serial.print(adxl_value[0]);
    Serial.print("  Y: ");Serial.print(adxl_value[1]);
    Serial.print("  Z: ");Serial.println(adxl_value[2]);
    
    delay(100);   // It is recommended to add delay between different I2C reads
    
    read_temperature();
//  Serial.print("Ambient = "); Serial.print(temp_ambient); 
    Serial.print("*F\tObject = "); Serial.print(temp_object); Serial.println("*F");
    delay(100);
    print_hr_data();
    print_gps_data();
  
    // Publish sensor data
    sprintf(buf, "ID:%s,X:%d.%02d,Y:%d.%02d,Z:%d.%02d,T:%d.%02d,H:%d,HV:%d,S:%d,SV:%d,LA:%d.%04d,LO:%d.%04d", 
        uuid, 
        (int)adxl_value[0], abs((int)(adxl_value[0]*100)%100),
        (int)adxl_value[1], abs((int)(adxl_value[1]*100)%100),
        (int)adxl_value[2], abs((int)(adxl_value[2]*100)%100),
        (int)temp_object, abs((int)(temp_object*100)%100),
        n_heart_rate, ch_hr_valid, n_spo2, ch_spo2_valid,
        (int)flat, abs((int)(flat*1000000)%1000000),
        (int)flon, abs((int)(flon*1000000)%1000000));
        
    Serial.println(buf);
    client.publish(mqtt_topic, buf);
  //  client.publish(mqtt_topic, "This is the test message from ear tag...");
    count = 0;
    WiFi.forceSleepBegin();
    delay(1000);
    ESP.deepSleep(10 * 1000000);
    delay(1000);
  }
  smartdelay(1000);
}
  
/* -------------------------------- MAXREFDES117# --------------------------------------------------- */
void print_hr_data(){
  un_min = 0x3FFFF;
  un_max = 0;
  n_ir_buffer_length = 100;  //buffer length of 100 stores 4 seconds of samples running at 25sps

//  Serial.println(digitalRead(INT_PIN));
  
  if (b_first){
    b_first = false;
    un_brightness = 0;     
  
//  read the first 100 samples, and determine the signal range
    for(i = 0; i < n_ir_buffer_length; i++){
//    while(digitalRead(MAX_INT_PIN) == 1){
//      delay(1);
//    }
      maxim_max30102_read_fifo((aun_red_buffer+i), (aun_ir_buffer+i));  //read from MAX30102 FIFO
      if(un_min > aun_red_buffer[i])
        un_min = aun_red_buffer[i];  //update signal min
      if(un_max < aun_red_buffer[i])
        un_max = aun_red_buffer[i];  //update signal max
//      Serial.print(F("red=")); Serial.print(aun_red_buffer[i], DEC);
//      Serial.print(F(", ir=")); Serial.println(aun_ir_buffer[i], DEC);
    }
    un_prev_data = aun_red_buffer[i];
    //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
    maxim_heart_rate_and_oxygen_saturation(aun_ir_buffer, n_ir_buffer_length, aun_red_buffer, &n_spo2, &ch_spo2_valid, &n_heart_rate, &ch_hr_valid); 
  
    Serial.print(F("HR="));Serial.print(n_heart_rate, DEC);
    Serial.print(F(", HRvalid="));Serial.print(ch_hr_valid, DEC);
    Serial.print(F(", SPO2="));Serial.print(n_spo2, DEC);
    Serial.print(F(", SPO2Valid="));Serial.println(ch_spo2_valid, DEC);
  }
  else{
    //dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
    for(i = 25; i < 100; i++){
      aun_red_buffer[i-25] = aun_red_buffer[i];
      aun_ir_buffer[i-25] = aun_ir_buffer[i];
  
      //update the signal min and max
      if(un_min > aun_red_buffer[i])
        un_min = aun_red_buffer[i];
      if(un_max < aun_red_buffer[i])
        un_max = aun_red_buffer[i];
    }
  
    //take 25 sets of samples before calculating the heart rate.
    for(i=75; i<100; i++)
    {
      un_prev_data = aun_red_buffer[i-1];
//        while(digitalRead(MAX_INT_PIN) == 1){
//          delay(1);
//        }
      maxim_max30102_read_fifo((aun_red_buffer+i), (aun_ir_buffer+i));
      //calculate the brightness of the LED
      if(aun_red_buffer[i] > un_prev_data)
      {
        f_temp = aun_red_buffer[i]-un_prev_data;
        f_temp /= (un_max-un_min);
        f_temp *= MAX_BRIGHTNESS;
        f_temp = un_brightness-f_temp;
        if(f_temp < 0)
          un_brightness = 0;
        else
          un_brightness = (int)f_temp;
      }
      else
      {
        f_temp = un_prev_data - aun_red_buffer[i];
        f_temp /= (un_max-un_min);
        f_temp *= MAX_BRIGHTNESS;
        un_brightness += (int)f_temp;
        if(un_brightness > MAX_BRIGHTNESS)
          un_brightness = MAX_BRIGHTNESS;
      }
    
      //send samples and calculation result to terminal program through UART
//      Serial.print(F("red=")); Serial.print(aun_red_buffer[i], DEC);
//      Serial.print(F(", ir=")); Serial.print(aun_ir_buffer[i], DEC);
    }
    maxim_heart_rate_and_oxygen_saturation(aun_ir_buffer, n_ir_buffer_length, aun_red_buffer, &n_spo2, &ch_spo2_valid, &n_heart_rate, &ch_hr_valid); 
    Serial.print(F("HR=")); Serial.print(n_heart_rate, DEC);
    Serial.print(F(", HRvalid=")); Serial.print(ch_hr_valid, DEC);
    Serial.print(F(", SPO2=")); Serial.print(n_spo2, DEC);
    Serial.print(F(", SPO2Valid=")); Serial.println(ch_spo2_valid, DEC);
  }
}


/*  -------------------------------- ADXL345 --------------------------------------------------- */
void initialize_accel(){
  Serial.println("-- Initializing ADXL345 module...");

  //ADXL345
  // i2c bus SDA = GPIO0; SCL = GPIO2
//  Wire.begin();

  // Put the ADXL345 into +/- 2G range by writing the value 0x01 to the DATA_FORMAT register.
  // FYI: 0x00 = 2G, 0x01 = 4G, 0x02 = 8G, 0x03 = 16G
  accel_write(DATA_FORMAT, 0x00);

  // Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
  accel_write(POWER_CTL, 0x08);

  // Get 10 values and calculate calibration values.
  uint8_t i = 0;
  uint8_t j = 0;
  for(i = 0; i < 11; i++)
  {
    readAccel(false);
    for (j = 0; j < 3; j++){
    adxl_cal[j] += adxl_value[j];
  }
    delay(100);
  }

  for (j = 0; j < 3; j++){
    adxl_cal[j] /= 10;
  }

  Serial.print("-- Calibration values of ADXL345 ->  X: ");Serial.print(adxl_cal[0]);
  Serial.print("  Y: ");Serial.print(adxl_cal[1]);
  Serial.print("  Z: ");Serial.println(adxl_cal[2]);

}

// Read X, Y, Z values and store to "adxl_value" global variable...
void readAccel(bool cal)
{
//  Serial.print("readAccel");
  uint8_t howManyBytesToRead = 6; //6 for all axes
  accel_read(DATAX0, howManyBytesToRead, adxl_buff); //read the acceleration data from the ADXL345
  short x = 0;
  x = (((short)adxl_buff[1]) << 8) | adxl_buff[0];
  adxl_value[0] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[3]) << 8) | adxl_buff[2];
  adxl_value[1] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[5]) << 8) | adxl_buff[4];
  adxl_value[2] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;

  if (cal == true)
    for (int j = 0; j < 3; j++){
      adxl_value[j] -= adxl_cal[j];
    }
}

void accel_write(byte address, byte val)
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // send register address
  Wire.write(val); // send value to write
  Wire.endTransmission(); // end transmission
}

// Reads num bytes starting from address register on device in to _buff array
void accel_read(byte address, int num, byte _buff[])
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // sends address to read from
  Wire.endTransmission(); // end transmission
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.requestFrom(DEVICE, num); // request 6 bytes from device

  int i = 0;
  while(Wire.available()) // device may send less than requested (abnormal)
  {
    _buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); // end transmission
}

/* ---------------------------------------- MLX96014 -------------------------------- */
//void read_mlx96014(){
//  // Read and print out the temperature
////  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC()); 
////  Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
//  temp_ambient = mlx.readAmbientTempF();
//  temp_object = mlx.readObjectTempF();
//  while (temp_ambient > 500){
//    temp_ambient = mlx.readAmbientTempF();
//    temp_object = mlx.readObjectTempF();
//  }
//}

/* ---------------------------------------- GPS -------------------------------- */

void print_gps_data(){
  gps.f_get_position(&flat, &flon, &age);  
  Serial.print("LAT: "); Serial.println(flat, 6);
  Serial.print("LON: "); Serial.println(flon, 6);
}

static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

bool maxim_max30102_write_reg(uint8_t uch_addr, uint8_t uch_data)
{
//  Serial.print("Writing register:: addr => ");Serial.print(uch_addr);Serial.print(", val => ");Serial.print(uch_data);
  Wire.beginTransmission(I2C_ADDR);
  Wire.write(uch_addr);
  Wire.write(uch_data);
  tmp = Wire.endTransmission();
//  Serial.print(", Result: ");Serial.println(tmp);
  return true;
}

bool maxim_max30102_read_reg(uint8_t uch_addr, uint8_t *puch_data)
{
//  Serial.print("Reading register :: Addr => ");Serial.print(uch_addr);Serial.print(", Read count => ");
  Wire.beginTransmission(I2C_ADDR);
  Wire.write(uch_addr);
  Wire.endTransmission(false);
  tmp = Wire.requestFrom(I2C_ADDR, 1);  // Returns the byte count of reading data
//  Serial.print(tmp);Serial.print(", Val => ");
  *puch_data = Wire.read();
//  Serial.println(*puch_data);
  return true;
}

bool maxim_max30102_init()
{
  if(!maxim_max30102_write_reg(REG_INTR_ENABLE_1,0xc0)) // INTR setting
    return false;
  if(!maxim_max30102_write_reg(REG_INTR_ENABLE_2,0x00))
    return false;
  if(!maxim_max30102_write_reg(REG_FIFO_WR_PTR,0x00))  //FIFO_WR_PTR[4:0]
    return false;
  if(!maxim_max30102_write_reg(REG_OVF_COUNTER,0x00))  //OVF_COUNTER[4:0]
    return false;
  if(!maxim_max30102_write_reg(REG_FIFO_RD_PTR,0x00))  //FIFO_RD_PTR[4:0]
    return false;
  if(!maxim_max30102_write_reg(REG_FIFO_CONFIG,0x4f))  //sample avg = 4, fifo rollover=false, fifo almost full = 17
    return false;
  if(!maxim_max30102_write_reg(REG_MODE_CONFIG,0x03))   //0x02 for Red only, 0x03 for SpO2 mode 0x07 multimode LED
    return false;
  if(!maxim_max30102_write_reg(REG_SPO2_CONFIG,0x27))  // SPO2_ADC range = 4096nA, SPO2 sample rate (100 Hz), LED pulseWidth (411uS)
    return false;
  
  if(!maxim_max30102_write_reg(REG_LED1_PA,0x24))   //Choose value for ~ 7mA for LED1
    return false;
  if(!maxim_max30102_write_reg(REG_LED2_PA,0x24))   // Choose value for ~ 7mA for LED2
    return false;
  if(!maxim_max30102_write_reg(REG_PILOT_PA,0x7f))   // Choose value for ~ 25mA for Pilot LED
    return false;
  return true;  
}

bool maxim_max30102_read_fifo(uint32_t *pun_red_led, uint32_t *pun_ir_led)
{
  uint8_t un_temp;
  uint8_t uch_temp;
  *pun_ir_led = 0;
  *pun_red_led = 0;
  maxim_max30102_read_reg(REG_INTR_STATUS_1, &uch_temp);
  maxim_max30102_read_reg(REG_INTR_STATUS_2, &uch_temp);
  Wire.beginTransmission(I2C_ADDR);
  Wire.write(REG_FIFO_DATA);
  Wire.endTransmission(false);
  Wire.requestFrom(I2C_ADDR, 6);  
  un_temp = Wire.read();
  un_temp <<= 16;
  *pun_red_led += un_temp;
  un_temp = Wire.read();
  un_temp <<= 8;
  *pun_red_led += un_temp;
  un_temp = Wire.read();
  *pun_red_led += un_temp;
  
  un_temp = Wire.read();
  un_temp <<= 16;
  *pun_ir_led += un_temp;
  un_temp = Wire.read();
  un_temp <<= 8;
  *pun_ir_led += un_temp;
  un_temp = Wire.read();
  *pun_ir_led += un_temp;
  *pun_red_led &= 0x03FFFF;  //Mask MSB [23:18]
  *pun_ir_led &= 0x03FFFF;  //Mask MSB [23:18]
  return true;
}

bool maxim_max30102_reset()
{
    if(!maxim_max30102_write_reg(REG_MODE_CONFIG, 0x40))
        return false;
    else
        return true;    
}

void read_temperature(){
  // Read temperature from MLX96014
  int attempt = 0;
  float temp;
  while (true){
    uint16_t ret;
    Wire.beginTransmission(0x5A); // start transmission to device 
    Wire.write(0x07); // sends register address to read from
    Wire.endTransmission(false); // end transmission
    
    Wire.requestFrom((uint8_t)0x5A, (uint8_t)3);// send data n-bytes read
    ret = Wire.read(); // receive DATA
    ret |= Wire.read() << 8; // receive DATA
    uint8_t pec = Wire.read();
    
    temp = ret;
    temp *= .02;
    temp  -= 273.15;
    temp = (temp* 9 / 5) + 32;  
    if (temp < 1000)
      break;
    else if (attempt > 5){
      temp = 0;
      break;
    }
    else{
      delay(10);
      attempt ++;
    }
  }
  
  temp_object = temp;
}

